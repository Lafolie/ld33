local fix = {}

local w, h
local sw, sh
local scale, ox, oy

function fix.set(width, height)
	w, h = width, height
end

function fix.resize(width, height)
	sw, sh = width, height

	local scalex = math.floor(sw/w)
	local scaley = math.floor(sh/h)
	scale = math.min(scalex, scaley)

	ox = math.floor((sw-w*scale)/2)
	oy = math.floor((sh-h*scale)/2)
end

function fix.apply()
	love.graphics.translate(ox, oy)
	love.graphics.scale(scale, scale)
end

function fix.scissor(enable)
	if enable then
		love.graphics.setScissor(ox, oy, w*scale, h*scale)
	else
		love.graphics.setScissor()
	end
end

function fix.scale(...)
	local out = {}
	local n = select("#", ...)
	for i = 1, n do
		out[i] = select(i, ...)*scale
	end
	return unpack(out, 1, n)
end

return fix
