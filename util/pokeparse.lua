local pokeparse = {}

function pokeparse.save(v)
	local t =
	{
		hp = v.hp,
		name = v.name,
		level = v.level,
		xp = v.xp,
		humanType = v.humanType,

		--abilityNameN
		--abilityTypeN
		--abilityPPN
		--abilityMaxPPN
	}

	for j, w in ipairs(v.abilities) do
		t["abilityName" .. j] = w.name
		t["abilityType" .. j] = w.type
		t["abilityPP" .. j] = w.pp
		t["abilityMaxPP" .. j] = w.maxPP
	end

	return t
end

function pokeparse.parse(pok, v)
	pok.hp = v.hp
	pok.name = v.name
	pok.level = v.level
	pok.xp = v.xp
	pok.humanType = v.humanType

	pok.abilities = {}
	for j = 1, 4 do
		local ability = {}
		ability.name = v["abilityName"..j]
		if not ability.name then break end
		ability.type = v["abilityType"..j]
		ability.pp = v["abilityPP"..j]
		ability.maxPP = v["abilityMaxPP"..j]

		pok.abilities[j] = ability
	end

	return pok
end

return pokeparse
