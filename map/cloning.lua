return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.13.0",
  orientation = "orthogonal",
  width = 17,
  height = 10,
  tilewidth = 16,
  tileheight = 16,
  nextobjectid = 3,
  properties = {},
  tilesets = {
    {
      name = "Tilez",
      firstgid = 1,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "gfx/tileset.png",
      imagewidth = 128,
      imageheight = 384,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 192,
      tiles = {
        {
          id = 8,
          properties = {
            ["solid"] = "true"
          }
        },
        {
          id = 43,
          properties = {
            ["solid"] = "true"
          }
        },
        {
          id = 45,
          properties = {
            ["solid"] = "true"
          }
        },
        {
          id = 47,
          properties = {
            ["solid"] = "true"
          }
        },
        {
          id = 51,
          properties = {
            ["solid"] = "true"
          }
        },
        {
          id = 52,
          properties = {
            ["solid"] = "true"
          }
        },
        {
          id = 53,
          properties = {
            ["solid"] = "true"
          }
        },
        {
          id = 55,
          properties = {
            ["solid"] = "true"
          }
        },
        {
          id = 61,
          properties = {
            ["solid"] = "true"
          }
        },
        {
          id = 62,
          properties = {
            ["solid"] = "true"
          }
        },
        {
          id = 63,
          properties = {
            ["solid"] = "true"
          }
        }
      }
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "Ground",
      x = 0,
      y = 0,
      width = 17,
      height = 10,
      visible = true,
      opacity = 1,
      properties = {
        ["solid"] = "true"
      },
      encoding = "lua",
      data = {
        9, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 44, 9,
        9, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 9,
        9, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 9,
        9, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 9,
        9, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 9,
        9, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 9,
        9, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 9,
        9, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 9,
        9, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 32, 9,
        9, 9, 9, 9, 9, 9, 9, 9, 68, 9, 9, 9, 9, 9, 9, 9, 9
      }
    },
    {
      type = "tilelayer",
      name = "Overlay",
      x = 0,
      y = 0,
      width = 17,
      height = 10,
      visible = true,
      opacity = 1,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 60, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 59, 59, 70, 30, 45, 61, 0, 0, 0, 61, 30, 45, 72, 58, 58, 0,
        0, 67, 67, 54, 38, 53, 0, 0, 0, 0, 0, 38, 53, 56, 66, 66, 0,
        0, 0, 0, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 56, 0, 0, 0,
        0, 0, 0, 62, 63, 63, 63, 63, 63, 63, 63, 63, 63, 64, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 9, 9, 9, 9, 9, 9, 9, 0, 9, 9, 9, 9, 9, 9, 9, 0
      }
    },
    {
      type = "tilelayer",
      name = "Wires",
      x = 0,
      y = 0,
      width = 17,
      height = 10,
      visible = true,
      opacity = 1,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 31, 0, 0, 0, 0, 0, 0, 31, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 39, 0, 0, 0, 0, 0, 0, 39, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "objectgroup",
      name = "Trainers",
      visible = true,
      opacity = 1,
      properties = {
        ["solid"] = "true"
      },
      objects = {
        {
          id = 2,
          name = "Dr Green",
          type = "blubGreen",
          shape = "rectangle",
          x = 128,
          y = 24,
          width = 16,
          height = 24,
          rotation = 0,
          visible = true,
          properties = {
            ["behaviour"] = "none",
            ["quest"] = "heal",
            ["range"] = "3",
            ["vat"] = "12,2",
            ["wallhacking"] = "true"
          }
        }
      }
    },
    {
      type = "objectgroup",
      name = "Teleports",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          id = 1,
          name = "",
          type = "",
          shape = "rectangle",
          x = 128,
          y = 144,
          width = 16,
          height = 16,
          rotation = 0,
          visible = true,
          properties = {
            ["map"] = "overworld",
            ["target"] = "23,28"
          }
        }
      }
    }
  }
}
