local cache = require "lib.cache"
local inifile = require "lib.inifile"
local pokeparse = require "util.pokeparse"

local weakmultiplier, weaknesses = 1.5,
{
	-- unarmed units are weak to steel, flak and bionic
	unarmed = { "steel", "flak", "bionic" },
	steel = { "magic", "bionic" },
	magic = { "flak" },
	flak = { "steel", "psychic" },
	psychic = { "unarmed" },
	bionic = { "psychic" },
}

local resistancemultiplier, resistances = 0.5,
{
	unarmed = { "psychic" },
	steel = { "unarmed", "steel" },
	magic = { "magic" },
	flak = { "magic", "flak" },
	psychic = { "flak","psychic", "bionic"},
	bionic = { "bionic","steel" },
}

local humans = 
{
	["Warrior"] = 
	{
		baseHp = 40,
		baseAttack = 50,
		baseDefense = 50,
		baseMagicDefense = 30,
		baseMagic = 30,
		baseAgility = 50, --total = 250
		type = "steel",
		battleSprite = "gfx//humon/warriorBack.png",
		battleSpriteEnemy = "gfx/humon/warrior.png",
	},
	["Merc"] = 
	{
		baseHp = 35,
		baseAttack = 60,
		baseDefense = 40,
		baseMagicDefense = 30,
		baseMagic = 30,
		baseAgility = 55, --total = 250
		type = "flak",
		battleSprite = "gfx//humon/mercBack.png",
		battleSpriteEnemy = "gfx/humon/merc.png",
	},
	["Witch"] =
	{
		baseHp = 20,
		baseAttack = 30,
		baseDefense = 30,
		baseMagicDefense = 70,
		baseMagic = 80,
		baseAgility = 20,
		type = "magic",
		battleSprite = "gfx/humon/witchBack.png",
		battleSpriteEnemy = "gfx/humon/witch.png",
	},
	["Medium"] =
	{
		baseHp = 30,
		baseAttack = 20,
		baseDefense = 50,
		baseMagicDefense = 70,
		baseMagic = 60,
		baseAgility = 20,
		type = "psychic",
		battleSprite = "gfx/humon/mediumBack.png",
		battleSpriteEnemy = "gfx/humon/medium.png",
	},
}

-- TODO Not quite sure how to model strengths yet

do -- make the table slightly easier to use
	for _, t in pairs(weaknesses) do
		for i, v in ipairs(t) do
			t[v] = true
		end
	end
end

do -- make the table slightly easier to use
	for _, t in pairs(resistances) do
		for i, v in ipairs(t) do
			t[v] = true
		end
	end
end

local pokedex = inifile.parse("pokedex.ini")

class "Pokesapien"
{
	worldSprite = cache.image "gfx/pokesapien.png", -- salmon
	battleSprite = cache.image "gfx//humon/warriorBack.png",
	battleSpriteEnemy = cache.image "gfx/humon/warrior.png",
	battleInfo = cache.image "gfx/window/battleInfo.png",
	battleInfoEnemy = cache.image "gfx/window/battleEnemy.png",

	humanType = "Warrior",
	type = "unarmed",
	maxHp = 150,
	hp = 132,
	name = "MissingNo",
	abilities = nil,
	level = 30,
	xpToNextLvl = 1000,
	xp = 900,
	attack = 10,
	defence = 10,
	magicAttack = 10,
	magicDefense = 10,
	agility = 10,
	
	__init__ = function(self, preset)
		if preset then
			pokeparse.parse(self, pokedex[preset])
		end
		self:setStats()	
	end,
	
	setStats = function(self)
		local human = humans[self.humanType]
		self.maxHp = math.floor(((human.baseHp + 50) * self.level)/ 50 + 10)
		self.attack = math.floor(((human.baseAttack + 50) * self.level)/ 50 + 5)
		self.defence = math.floor(((human.baseDefense + 50) * self.level)/ 50 + 5)
		self.magicAttack = math.floor(((human.baseMagic + 50) * self.level)/ 50 + 5)
		self.magicDefense = math.floor(((human.baseMagicDefense + 50) * self.level)/ 50 + 5)
		self.agility = math.floor(((human.baseAgility + 50) * self.level)/ 50 + 5)
		self.type = human.type
		self.xpToNextLvl = (self.level^3 *4)/5 - ((self.level-1)^3 *4)/5
		if self.hp > self.maxHp then
			self.hp = self.maxHp
		end
		
		self.battleSprite = cache.image(human.battleSprite)
		self.battleSpriteEnemy = cache.image(human.battleSpriteEnemy)
		
		self.displayHp = self.hp
		self.displayXp = self.xp
		self.displayLevel = self.level
	end,
	
	isAlive = function(self)
		return self.hp > 0
	end,

	damage = function(self, type, amount)
		local weak = weaknesses[self.type][type]
		local resistant = resistances[self.type][type]
		
		if weak then
			amount = math.floor(amount * weakmultiplier)
		elseif resistant then
			amount = math.floor(amount * resistancemultiplier)
		else
			amount = math.floor(amount)
		end

		-- Some kind of signal system for the damage indicator?
		-- signal:send("damage", self, amount, weak)

		self.hp = math.max(self.hp - amount, 0)
		
		if weak then
			return "It's super effective!"
		elseif resistant then
			return "It's not very effective!"
		else
			return ""
		end
	end,
	
	gainExp = function(self,enemy)
		local expGain = math.floor((enemy.level * 200) / 7)
		local expGain2 = expGain
		local levels = 0
		while expGain2 ~= 0 do
			if expGain2 >= self.xpToNextLvl - self.xp then
				expGain2 = expGain2 - (self.xpToNextLvl - self.xp)
				self.level = self.level + 1
				levels = levels + 1
				self.xp = 0
				self:setStats()
			else
				self.xp = self.xp + expGain2
				expGain2 = 0
			end
		end
		local msg1 = self.name.." gained "..expGain.." EXP. Points!"
		local msg2 = self.name.." grew to LV. "..self.level.."!"
		if levels > 0 then
			return msg1,msg2
		else
			return msg1
		end
	end,
	
	update = function(self, dt)
		if self.displayHp ~= self.hp then
			if self.displayHp > self.hp then
				self.displayHp = math.floor(self.displayHp - (self.maxHp/10)*dt)
				if self.hp > self.displayHp then
					self.displayHp = self.hp
				end
			else
				self.displayHp = math.ceil(self.displayHp + (self.maxHp/10)*dt)
				if self.hp < self.displayHp then
					self.displayHp = self.hp
				end
			end
		end
		
		if self.displayLevel < self.level then
			if self.displayXp < self.xpToNextLvl then
				self.displayXp = math.ceil(self.displayXp + (self.xpToNextLvl/5)*dt)
				if self.xpToNextLvl < self.displayXp then
					self.displayXp = self.xpToNextLvl
				end
			else
				self.displayXp = 0
				self.displayLevel = self.displayLevel + 1
			end
		elseif self.displayXp < self.xp then
			self.displayXp = math.ceil(self.displayXp + (self.xpToNextLvl/5)*dt)
			if self.xp < self.displayXp then
				self.displayXp = self.xp
			end
		end
		
		
		
	end,

	drawInWorld = function(self, x, y)
		love.graphics.setColor(255, 255, 255)
		love.graphics.draw(self.worldSprite, x, y)
	end,

	drawHpBar = function(self, x, y, health)
		local perc = self.displayHp/self.maxHp
		if perc < 0.15 then
			love.graphics.setColor(173, 15, 15)
		elseif perc < 0.50 then
			love.graphics.setColor(223, 208, 48)
		else
			love.graphics.setColor(153, 232, 96)
		end

		love.graphics.rectangle('fill', x, y, 67*perc, 3)

		if health then
			love.graphics.printfs(self.displayHp .. "/" .. self.maxHp, x, y+4, 63, "right")
		end
	end,

	drawInBattle = function(self, offsetX, offsetY, drawHuman)
		local shakeOffsetY = 0
		local shakeOffsetX = 0
		local drawHuman = drawHuman ~= false
		local angle = 0
		if self.bob then
			shakeOffsetY = math.sin(love.timer.getTime()*4)
		elseif self.sway then
			angle = math.rad((math.sin(love.timer.getTime()*5)*7)-5)
		elseif self.violentShake then
			shakeOffsetX = math.sin(love.timer.getTime()*16)*2
			drawHuman = math.sin(love.timer.getTime()*30) < 0
		end
		
		love.graphics.setColor(255, 255, 255)
		if drawHuman then
			love.graphics.draw(self.battleSprite, 64 + offsetX + shakeOffsetX, 114 + offsetY + shakeOffsetY, angle, 1, 1, 64, 128)
		end
		love.graphics.draw(self.battleInfo, 110 + offsetX, 73 + offsetY)
		
		love.graphics.prints(self.name, 124 + offsetX , 76 + offsetY)
		love.graphics.prints("Lv"..self.level, 176 + offsetX , 76 + offsetY)
		
		self:drawHpBar(138 + offsetX, 88 + offsetY, true)

		love.graphics.setColor(0, 255, 255)
		love.graphics.rectangle('fill', 129 + offsetX, 105 + offsetY, (79/self.xpToNextLvl) * self.displayXp, 3)
		
	end,
	
	drawInBattleEnemy = function(self, offsetX, offsetY)
	
		local shakeOffsetY = 0
		local shakeOffsetX = 0
		local drawHuman = true
		local angle = 0
		if self.bob then
			shakeOffsetY = math.sin(love.timer.getTime()*4)
		elseif self.sway then
			angle = math.rad((math.sin(love.timer.getTime()*5)*7)-5)
		elseif self.violentShake then
			shakeOffsetX = math.sin(love.timer.getTime()*16)*2
		end
		
		if self.flash then
			drawHuman = math.sin(love.timer.getTime()*30) < 0
		end
	
	
	
		love.graphics.setColor(255, 255, 255)
		if drawHuman then
			love.graphics.draw(self.battleSpriteEnemy, 172 + offsetX + shakeOffsetX, 37 + offsetY + shakeOffsetY, angle, 1, 1, 32, 32)
		end
		love.graphics.draw(self.battleInfoEnemy, 20 + offsetX, 5 + offsetY)
		
		love.graphics.prints(self.name, 26 + offsetX , 8 + offsetY)
		love.graphics.prints("Lv"..self.level, 78 + offsetX , 8 + offsetY)
		
		self:drawHpBar(40 + offsetX, 20 + offsetY, false)
	end,
}
