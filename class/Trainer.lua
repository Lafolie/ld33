local anim = require "lib.anim8"
local convoke = require "lib.convoke"

require "class.Pokesapien"

class "Trainer"
{
	pokesapiens = nil,
	items = nil,
	facing = nil,
	name = "Human Trainer",

	__init__ = function(self, x, y)
		self.worldPosition = vector(x or 1, y or 1)
		self.facing = vector(0, 1)
		
		self.items =
		{
			{
				name = "Human Trap",
				qty = 10
			},
			{
				name = "Potion",
				qty = 5
			},
			{
				name = "Revive",
				qty = 5
			},
			{
				name = "Whip",
				qty = 5
			},
			--{ -- FIXME
			--	name = "D Bug",
			--	qty = 99,
			--},
		}
		self.battleSpriteScale = 1
		self.pokesapiensInStorage = {}
	end,

	createAnimation = function(self)
		local grid = anim.newGrid(16, 24, self.worldImage:getWidth(), self.worldImage:getHeight(), -1, -1, 1)
		local speed = 0.13
		self.worldAnims =
		{
			up    = anim.newAnimation(grid(2, 1, '1-3', 1), speed),
			down  = anim.newAnimation(grid(2, 2, '1-3', 2), speed),
			left  = anim.newAnimation(grid(2, 3, '1-3', 3), speed),
			right = anim.newAnimation(grid(2, 4, '1-3', 4), speed),
		}
	end,

	getDrawPosition = function(self)
		return self.drawPosition or self.worldPosition
	end,

	getDir = function(self)
		if self.facing.y == 1 then
			return "down"
		elseif self.facing.y == -1 then
			return "up"
		elseif self.facing.x == 1 then
			return "right"
		elseif self.facing.x == -1 then
			return "left"
		end
	end,

	drawInWorld = function(self)
		local pos = self:getDrawPosition()
		love.graphics.setColor(255, 255, 255)

		if self.worldAnims then
			self.worldAnims[self:getDir()]:draw(self.worldImage, (pos.x-1)*16, (pos.y-1)*16-8)
		else
			love.graphics.draw(self.worldImage, (pos.x-1)*16, (pos.y-1)*16-8)

			-- FIXME
			love.graphics.setColor(255, 0, 0)
			love.graphics.point((pos.x-1)*16+8+self.facing.x*10, (pos.y-1)*16+8+self.facing.y*10)
		end
	end,

	drawInBattle = function(self, offsetX, offsetY)
		love.graphics.draw(self.battleImage, 30 + offsetX, 50 + offsetY,0 ,self.battleSpriteScale,self.battleSpriteScale)
	end,
	
	drawInBattleEnemy = function(self, offsetX, offsetY)
		love.graphics.draw(self.battleImage, 140 + offsetX, 5 + offsetY,0 ,self.battleSpriteScale,self.battleSpriteScale)
	end,
	
	prepareForBattle = function(self)
		if not self.activeSapien then
			self.activeSapien = self:firstSapienAlive()
		end
	end,
	
	firstSapienAlive = function(self)
		for i, v in ipairs(self.pokesapiens) do
			if v:isAlive() then
				return v
			end
		end
	end,

	update = function(self, dt)
		if not self.worldAnims then return false end
		if self.drawPosition then
			self.worldAnims[self:getDir()]:update(dt)
		else
			self.worldAnims[self:getDir()]:gotoFrame(1)
		end
	end,

	move = function(self, movement)
		local oldpos = self.worldPosition
		local newpos = oldpos + movement
		self.facing = movement
		if states.world:canMoveTo(newpos.x, newpos.y) then
			convoke(function (continue, wait)
				self.moving = true
				self.worldPosition = newpos
				self.drawPosition = oldpos

				states.world.timer.tween(0.3, self.drawPosition, newpos,
						"linear", continue())
				wait()

				self.drawPosition = nil
				self.moving = false

				self:postmove()
			end)()
		end
	end,

	postmove = function(self)
	end,

	defeatCb = function(self)
	end,
}
