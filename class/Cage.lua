local cache = require "lib.cache"
local vector = require "lib.vector"

class "Cage"
{
	__init__ = function(self, x, y, cb)
		self.gfx = cache.image "gfx/mysteryBox.png"
		self.worldPosition = vector(x or 1, y or 1)
		self.dead = false
		self.cb = cb
		self.inMap = states.world.currentMap
	end,

	activate = function(self, player)
		if self.cb and self:cb() then
			return
		end
		if self.sapien then
			player:captureSapien(self.sapien)
			Gamestate.push(states.msgbox, "You got " .. self.sapien.name)
			self.sapien = nil
		else
			Gamestate.push(states.msgbox, "This cage is empty.")
		end
	end,

	drawInWorld = function(self)
		love.graphics.setColor(255, 255, 255)
		love.graphics.draw(self.gfx, (self.worldPosition.x-1)*16, (self.worldPosition.y-1)*16)
	end,
}
