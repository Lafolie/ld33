local cache = require "lib.cache"

require "class.Trainer"

class "Player" (Trainer)
{
	__init__ = function(self, x, y)
		Trainer.__init__(self, x, y)
		self.pokesapiens = {}

		self.worldImage = cache.image "gfx/monster/etherian.png"
		self.battleImage = cache.image("gfx/monster/etherianBattle.png")
		self:createAnimation()
	end,

	postmove = function(self)
		local tp = states.world:teleportAt(self.worldPosition)
		if tp then
			states.world:teleportPlayer(tp)
			return
		end

		local trainer = states.world:getRandomEncounter()
		if trainer then
			states.world:encounter(trainer)
		end
	end,

	captureSapien = function(self, sapien)
		if #self.pokesapiens < 6 then
			table.insert(self.pokesapiens, sapien)
		else
			table.insert(self.pokesapiensInStorage, sapien)
		end
	end,
}
