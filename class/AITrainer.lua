local cache = require "lib.cache"
local convoke = require "lib.convoke"

require "class.Trainer"
require "class.Pokesapien"
require "class.Cage"
require "class.Vat"

local encounterTexts =
{
	"I pity the fool!",
	"Get off my lawn!",
	"You shall pay for this with your life.",
}

local behaviours = {}
local quests = {}

class "AITrainer" (Trainer)
{
	range = 5,
	dialog = nil,
	seenPlayer = false,
	defeated = false,
	behaviour = "rotate",

	__init__ = function(self, type, x, y)
		Trainer.__init__(self, x, y)
		self.pokesapiens = {Pokesapien("Dave69"),Pokesapien("Dave23")}
		self.worldImage = cache.image("gfx/monster/" .. type .. ".png")
		self.battleImage = cache.image("gfx/monster/" .. type .. "Battle.png")
		if type ~= "etherian" then
			self.battleSpriteScale = 4
		end
		if self.worldImage:getWidth() > 16 then
			self:createAnimation()
		end

		self.items = {}
		self.timer = 0
	end,

	update = function(self, dt)
		Trainer.update(self, dt)
		if self.defeated then return end

		behaviours[self.behaviour](self, dt)

		if states.world:getPlayerDistance(self.worldPosition:unpack()) > self.range then
			self.seenPlayer = false
			return
		end

		local checkPos = self.worldPosition
		for i = 1, self.range do
			checkPos = checkPos + self.facing
			if not self.wallhacking and states.world:blocksView(checkPos:unpack()) then
				break
			end

			if states.world:isPlayer(checkPos:unpack()) then
				if not self.seenPlayer then
					self:spottedPlayer()
				end
				self.seenPlayer = true
				return
			end
		end

		self.seenPlayer = false
	end,

	getEncounterText = function(self)
		return encounterTexts[love.math.random(#encounterTexts)]
	end,

	spottedPlayer = function(self)
		if self.quest then
			quests[self.quest](self)
		else
			states.world:encounter(self)
		end
	end,

	defeatCb = function(self)
		if self.endboss then
			Gamestate.pushlate(states.gameEnd)
		end
	end,
}

function behaviours:rotate(dt)
	self.timer = self.timer + dt
	if self.timer >= 5 then
		self.timer = self.timer - 5
		self.facing:rotate_inplace(math.pi/2)
	end
end

local function lineco(self)
	local dir = vector(0, 0)
	dir[self.beh_axis] = 1
	self.beh_range = tonumber(self.beh_range) or 0

	for i = 1, self.beh_range do
		self:move(dir)
		coroutine.yield()
	end

	while true do
		dir = -dir
		for i = 1, 2*self.beh_range do
			self:move(dir)
			coroutine.yield()
		end
	end
end

function behaviours:line(dt)
	if not self.lineco then
		self.lineco = coroutine.wrap(lineco)
	end

	self.timer = self.timer + dt
	if self.timer >= 2 then
		self.timer = self.timer - 2
		self:lineco()
	end
end

local function circleco(self)
	local dir = vector(1, 0)
	self.beh_radius = tonumber(self.beh_radius) or 0

	while true do
		self:move(dir)
		coroutine.yield()

		for i = 1, self.beh_radius do
			self:move(dir)
			coroutine.yield()
		end

		dir:rotate_inplace(math.pi/2)
	end
end

function behaviours:circle(dt)
	if not self.circleco then
		self.circleco = coroutine.wrap(circleco)
	end

	self.timer = self.timer + dt
	if self.timer >= 2 then
		self.timer = self.timer - 2
		self:circleco()
	end
end

function behaviours:random(dt)
	self.timer = self.timer + dt
	if self.timer >= 2.5 then
		self.timer = self.timer - 2.5

		local dir = vector(0, 0)
		local axis = self.beh_axis
		if not axis then
			axis = love.math.random(0, 1) == 1 and "x" or "y"
		end
		dir[axis] = love.math.random(0, 1)*2-1

		self:move(dir)
	end
end

function behaviours:none(dt)
end

function quests:start()
	if self.queststarted or self.defeated then return end
	convoke(function(continue, wait)
		self.queststarted = true
		Gamestate.pushlate(states.msgbox, "Welcome, Trainer! I am " .. self.name .. ".", continue())
		wait()

		Gamestate.pushlate(states.msgbox, "As listed in the brochure, your visit to Erf comes with a complimentary Humon.", continue())
		wait()

		local cages = {}
		for x = 18, 20 do
			local cage = Cage(x, 11)
			cage.sapien = Pokesapien("Starter"..x-17)
			table.insert(states.world.cages, cage)
			table.insert(cages, cage)
		end

		Gamestate.pushlate(states.msgbox, "I've laid out 3 cages, pick one.", continue())
		wait()

		local pick
		while true do
			local cont = continue()
			for i, v in ipairs(cages) do
				v.cb = function(self) cont(self) return true end
			end
			pick = wait().sapien

			Gamestate.pushlate(states.msgbox, "Do you want to pick " .. pick.name .. "\na "..pick.type.." type Humon?", continue())
			wait()
			Gamestate.pushlate(states.question, continue())
			if wait() then
				break
			end
		end

		for i, v in ipairs(cages) do
			v.dead = true
		end
		Gamestate.pushlate(states.msgbox, pick.name .. " is a very good choice!")
		states.world.player:captureSapien(pick)

		self.defeated = true
	end)()
end

function quests:heal()
	convoke(function(continue, wait)
		Gamestate.pushlate(states.msgbox, "Good day, I'm " .. self.name .. "!", continue())
		wait()

		Gamestate.pushlate(states.msgbox, "We'll heal your live humons, and replace your dead ones with fresh clones.", continue())
		wait()

		local vatX, vatY, vat
		if self.vat then
			vatX, vatY = self.vat:match("(%d+),(%d+)")
			if vatX then
				vatX, vatY = tonumber(vatX), tonumber(vatY)
			end
		end

		if vatX then
			vat = Vat(vatX, vatY)
			table.insert(states.world.vats, vat)
		end

		states.world.player.frozen = true
		Timer.add(3, continue())
		wait()

		if vat then
			vat.dead = true
		end

		for i, v in ipairs(states.world.player.pokesapiens) do
			v.hp = v.maxHp
			v.displayHp = v.hp
			for j, w in ipairs(v.abilities) do
				w.pp = w.maxPP
			end
		end

		states.world.player.frozen = false
		Gamestate.pushlate(states.msgbox, "Thank you, come again! And don't forget, you local Cloning Center does free humon disposal.")
	end)()
end
