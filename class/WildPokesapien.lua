require "class.Trainer"
local cache = require "lib.cache"

class "WildPokesapien" (Trainer)
{
	__init__ = function(self, pokesapien)
		Trainer.__init__(self, 0, 0)
		self.pokesapiens = {pokesapien}
		
		self.battleImage = cache.image("gfx/monster/etherianBattle.png")
		-- will be drawn off screen
	end,

	getEncounterText = function(self)
		return ("A wild %s appears!"):format(self.pokesapiens[1].name)
	end,
}
