local anim = require "lib.anim8"
local vector = require "lib.vector"
local cache = require "lib.cache"

class "Vat"
{
	__init__ = function(self, x, y)
		self.worldPosition = vector(x, y)

		self.sheet = cache.image "gfx/tileset.png"
		self.grid = anim.newGrid(16, 32, self.sheet:getWidth(), self.sheet:getHeight(), 32, 144)
		self.stationary = self.grid(1, 1)[1]
		self.anim = anim.newAnimation(self.grid('2-3', 1), 0.2)

		self.timer = 0
	end,

	update = function(self, dt)
		self.timer = self.timer + dt
		self.anim:update(dt)
	end,

	drawInWorld = function(self)
		love.graphics.setColor(255, 255, 255)
		local x, y = (self.worldPosition.x-1)*16, (self.worldPosition.y-1)*16
		if self.timer < 0.3 or self.timer > 2.7 then
			love.graphics.draw(self.sheet, self.stationary, x, y)
		else
			self.anim:draw(self.sheet, x, y)
		end
	end,
}
