local requireall = require "util.requireall"
local resolutionfix = require "util.resolutionfix"

-- Globals, woo
class = require "lib.slither"
vector = require "lib.vector"
Gamestate = require "lib.gamestate"
Timer = require "lib.timer"
states = requireall "state"
WIDTH, HEIGHT = 240, 160

love.graphics.setDefaultFilter("nearest", "nearest")

requireall "class"

function love.load(args)
	love.graphics.setNewFont("font/pokemon_generation_1.ttf", 8)

	resolutionfix.set(WIDTH, HEIGHT)
	resolutionfix.resize(love.graphics.getDimensions())

	local game, state = unpack(args)
	Gamestate.registerEvents()
	Gamestate.switch(states[state] or states.intro)
end

function love.resize(w, h)
	resolutionfix.resize(w, h)
end

function love.update(dt)
	Timer.update(dt)
end

function love.draw()
	resolutionfix.apply()
end

function love.graphics.prints(text, x, y, ...)
	love.graphics.setColor(0, 0, 0)
	love.graphics.print(text, x, y+1, ...)

	love.graphics.setColor(255, 255, 255)
	love.graphics.print(text, x, y, ...)
end

function love.graphics.printfs(text, x, y, ...)
	love.graphics.setColor(0, 0, 0)
	love.graphics.printf(text, x, y+1, ...)

	love.graphics.setColor(255, 255, 255)
	love.graphics.printf(text, x, y, ...)
end

function love.graphics.printd(text, x, y, ...)
	love.graphics.setColor(0, 0, 0)
	love.graphics.print(text, x, y+1, ...)

	love.graphics.setColor(100, 100, 100)
	love.graphics.print(text, x, y, ...)
end

function love.graphics.printfd(text, x, y, ...)
	love.graphics.setColor(0, 0, 0)
	love.graphics.printf(text, x, y+1, ...)

	love.graphics.setColor(100, 100, 100)
	love.graphics.printf(text, x, y, ...)
end
