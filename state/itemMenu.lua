local cache = require "lib.cache"

local itemMenu = {}

function itemMenu:enter(parent, player)
	self.parent = parent
	self.player = player
	self.selection = 1
	
	self.bg1 = cache.image "gfx/window/standard.png"
	self.bg2 = cache.image "gfx/window/item.png"
end


function itemMenu:keypressed(key)
	if key == "escape" then
		Gamestate.poplate()
	elseif key == "down" then
		self.selection = self.selection % #self.player.items + 1
	elseif key == "up" then
		self.selection = (self.selection - 2) % #self.player.items + 1
	elseif key == "return" then
		self:useItem()
		
	end
end

function itemMenu:useItem()
	Gamestate.poplate(self.player.items[self.selection])
end

function itemMenu:draw()
	self.parent.parent:draw()
	

	
	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(self.bg1, 0, HEIGHT-48)
	love.graphics.draw(self.bg2, WIDTH - 112, 0)

	love.graphics.setColor(0, 0, 0)
	for i, v in ipairs(self.player.items) do
		if self.selection == i then
			local offset = math.sin(love.timer.getTime()*4)
			love.graphics.setColor(0, 0, 0)
			love.graphics.polygon('fill', (WIDTH-112) + 6, i*15 - 9,
			(WIDTH-112) + 6, i*15 + 1,
			(WIDTH-112) + 11, i*15 - 1)
			
			love.graphics.setColor(255, 255, 255)
			
			love.graphics.polygon('fill', (WIDTH-112) + 5, i*15 -10,
			(WIDTH-112) + 5, i*15 ,
			(WIDTH-112) + 10, i*15 -5)
			love.graphics.prints(v.name, (WIDTH-112) + 10 + offset , i*15 -10)
			love.graphics.prints("x"..v.qty, (WIDTH-26) + offset , i*15 -10)
			
		else
			love.graphics.prints(v.name, (WIDTH-112) + 10, i*15 -10)
			love.graphics.prints("x"..v.qty, (WIDTH-26), i*15 -10)
		end
	end
end

return itemMenu
