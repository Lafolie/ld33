local cache = require "lib.cache"
local convoke = require "lib.convoke"

local itemMenuWorld = {}

itemEffectsWorld =
{
	["Potion"] = function(pokesapien)
		if pokesapien:isAlive() then
			pokesapien.hp = math.min(math.floor(pokesapien.hp + pokesapien.maxHp/3),pokesapien.maxHp)
			pokesapien.displayHp = pokesapien.hp
			return  pokesapien.name.." used Potion\nhealing a small amount" 
		else
			return "Potion had no effect It's already dead!"
		end
	end,
	["Whip"] = function(pokesapien)
		if pokesapien:isAlive() then
			pokesapien.hp = pokesapien.hp - 1
			pokesapien.xp = pokesapien.xp + 10
			return  "Get stronger or I'll have you disposed!"
		else
			return "No point flogging a dead Humon!"
		end
	end,
	["Revive"] = function(pokesapien)
		if pokesapien:isAlive() then
			return  "Revive had no effect It's not dead yet!"
		else
			pokesapien.hp = math.floor(pokesapien.maxHp/3)
			return  pokesapien.name.." is ready to battle again!"
		end
	end,
}


function itemMenuWorld:enter(parent, player)
	self.parent = parent
	self.player = player
	self.selection = 1
	
	self.bg = cache.image "gfx/window/item.png"
end


function itemMenuWorld:keypressed(key)
	if key == "escape" then
		Gamestate.poplate()
	elseif key == "down" then
		self.selection = self.selection % #self.player.items + 1
	elseif key == "up" then
		self.selection = (self.selection - 2) % #self.player.items + 1
	elseif key == "return" then
		self:useItem()
		
	end
end

function itemMenuWorld:useItem()
	local item = self.player.items[self.selection]
	if item == nil then return end
	local itemFunc = itemEffectsWorld[item.name]
	if itemFunc ~= nil then
		convoke(function(continue, wait)
			Gamestate.pushlate(states.msgbox, "Choose a Humon to use "..item.name.." on!", continue())
			wait()
			Gamestate.pushlate(states.humonSelectionMenu, states.world.player)
			end)()
	else
		Gamestate.pushlate(states.msgbox, "Your cannot use "..item.name.." outside of battle!")
	end
	
end

function itemMenuWorld:resume(from, pokesapien)
	if from == states.humonSelectionMenu and pokesapien then
		local item = self.player.items[self.selection]
		local itemFunc = itemEffectsWorld[item.name]
		local msg = itemFunc(pokesapien)
		item.qty = item.qty-1
		if item.qty == 0 then
			for i, v in ipairs(self.player.items) do
				if v == item then
					table.remove(self.player.items,i)
				end
			end
		end
		Gamestate.pushlate(states.msgbox, msg)	
	end
end


function itemMenuWorld:draw()
	self.parent.parent:draw()
	

	
	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(self.bg, WIDTH - 112, 0)

	love.graphics.setColor(0, 0, 0)
	for i, v in ipairs(self.player.items) do
		if self.selection == i then
			local offset = math.sin(love.timer.getTime()*4)
			love.graphics.setColor(0, 0, 0)
			love.graphics.polygon('fill', (WIDTH-112) + 6, i*15 - 9,
			(WIDTH-112) + 6, i*15 + 1,
			(WIDTH-112) + 11, i*15 - 1)
			
			love.graphics.setColor(255, 255, 255)
			
			love.graphics.polygon('fill', (WIDTH-112) + 5, i*15 -10,
			(WIDTH-112) + 5, i*15 ,
			(WIDTH-112) + 10, i*15 -5)
			love.graphics.prints(v.name, (WIDTH-112) + 10 + offset , i*15 -10)
			love.graphics.prints("x"..v.qty, (WIDTH-26) + offset , i*15 -10)
			
		else
			love.graphics.prints(v.name, (WIDTH-112) + 10, i*15 -10)
			love.graphics.prints("x"..v.qty, (WIDTH-26), i*15 -10)
		end
	end
end

return itemMenuWorld
