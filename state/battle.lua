local cache = require "lib.cache"
local convoke = require "lib.convoke"

local battle = {}

itemEffects =
{
	["Human Trap"] = function(attackingPokesapien,defendingPokesapien,item)
		if not battle.wild then
			battle:nextTurn()
			item.qty = item.qty+1
			return "You cannot capture another trainers Humon!",1 
		else
			if (defendingPokesapien.hp < defendingPokesapien.maxHp/2 and love.math.random(10) > 5) or defendingPokesapien.hp < defendingPokesapien.maxHp/4 and love.math.random(10) > 5  then
				battle.captureSuccess = true
			else
				battle.captureFailed = true
			end
			return "You attempt to capture "..defendingPokesapien.name,4
		end
	end,
	["Potion"] = function(attackingPokesapien,defendingPokesapien)
		if attackingPokesapien:isAlive() then
			attackingPokesapien.hp = math.min(math.floor(attackingPokesapien.hp + attackingPokesapien.maxHp/3),attackingPokesapien.maxHp)
			return  attackingPokesapien.name.." used Potion\nhealing a small amount",2
		else
			return "Potion had no effect It's already dead!",2
		end
	end,
	["Whip"] = function(attackingPokesapien,defendingPokesapien)
		attackingPokesapien.hp = attackingPokesapien.hp - 1
		attackingPokesapien.xp = attackingPokesapien.xp + 10
			return  "Get stronger or I'll have you disposed!",2
	end,
	["Revive"] = function(attackingPokesapien,defendingPokesapien)
		if attackingPokesapien:isAlive() then
			return  "Revive had no effect It's not dead yet!",2
		else
			attackingPokesapien.hp = math.floor(attackingPokesapien.maxHp/3)
			return  attackingPokesapien.name.." is ready to battle again!",2
		end
	end,
	["D Bug"] = function(attackingPokesapien, defendingPokesapien, item)
		battle.lose = true
		return "Whatever!", 1
	end
}

damageCalc = function (lvl,attack,defence,base) 
	return ((((2 * lvl +10)/250) * (attack/defence) * base) + 2)*(1-(love.math.random(15)/100))
end

attackDetails = 
{
	["Stab"] = 
	{
		damageCalc = function(attackingPokesapien,defendingPokesapien)
			return damageCalc(attackingPokesapien.level, attackingPokesapien.attack,defendingPokesapien.defence,60)
		end,
		priority = 0
	},
	["Fast Hit"] = 
	{
		damageCalc = function(attackingPokesapien,defendingPokesapien)
			return damageCalc(attackingPokesapien.level, attackingPokesapien.attack,defendingPokesapien.defence,50)
		end,
		priority = 1
	},
	["Bow Attack"] = 
	{
		damageCalc = function(attackingPokesapien,defendingPokesapien)
			return damageCalc(attackingPokesapien.level, attackingPokesapien.attack,defendingPokesapien.defence,50)
		end,
		priority = 0
	},
	["Punch"] = 
	{
		damageCalc = function(attackingPokesapien,defendingPokesapien)
			return damageCalc(attackingPokesapien.level, attackingPokesapien.attack,defendingPokesapien.defence,50)
		end,
		priority = 0
	},
	["Full Volley"] = 
	{
		damageCalc = function(attackingPokesapien,defendingPokesapien)
			return damageCalc(attackingPokesapien.level, attackingPokesapien.attack,defendingPokesapien.defence,60)
		end,
		priority = 0
	},
	["Quick Fire"] = 
	{
		damageCalc = function(attackingPokesapien,defendingPokesapien)
			return damageCalc(attackingPokesapien.level, attackingPokesapien.attack,defendingPokesapien.defence,50)
		end,
		priority = -2
	},
	["Headshot"] = 
	{
		damageCalc = function(attackingPokesapien,defendingPokesapien)
			return damageCalc(attackingPokesapien.level, attackingPokesapien.attack,defendingPokesapien.defence,80)
		end,
		priority = -2
	},
	["Borg Strike"] = 
	{
		damageCalc = function(attackingPokesapien,defendingPokesapien)
			return damageCalc(attackingPokesapien.level, attackingPokesapien.attack,defendingPokesapien.defence,60)
		end,
		priority = 0
	},
	["Plasma"] =
	{
		damageCalc = function(attackingPokesapien,defendingPokesapien)
			return damageCalc(attackingPokesapien.level, attackingPokesapien.magicAttack, defendingPokesapien.magicDefense, 55)
		end,
		priority = 1
	},
	["Inferno"] = 
	{
		damageCalc = function(attackingPokesapien,defendingPokesapien)
			return damageCalc(attackingPokesapien.level, attackingPokesapien.magicAttack,defendingPokesapien.magicDefense,60)
		end,
		priority = 0
	},
	["Staff Hit"] = 
	{
		damageCalc = function(attackingPokesapien,defendingPokesapien)
			return damageCalc(attackingPokesapien.level, attackingPokesapien.attack,defendingPokesapien.defence,40)
		end,
		priority = 0
	},
	["Omega"] = 
	{
		damageCalc = function(attackingPokesapien,defendingPokesapien)
			return damageCalc(attackingPokesapien.level, attackingPokesapien.magicAttack,defendingPokesapien.magicDefense,70)
		end,
		priority = -3
	},
	["VoltHands"] =
	{
		damageCalc = function(attackingPokesapien,defendingPokesapien)
			return damageCalc(attackingPokesapien.level, attackingPokesapien.magicAttack, defendingPokesapien.magicDefense, 60)
		end,
		priority = 0
	},
	["RapidPunch"] =
	{
		damageCalc = function(attackingPokesapien,defendingPokesapien)
			return damageCalc(attackingPokesapien.level, attackingPokesapien.attack, defendingPokesapien.defence, 50)
		end,
		priority = 0
	},
	["Soul Strike"] =
	{
		damageCalc = function(attackingPokesapien,defendingPokesapien)
			return damageCalc(attackingPokesapien.level, attackingPokesapien.magicAttack, defendingPokesapien.defence, 60)
		end,
		priority = -1
	},
	["Electrify"] =
	{
		damageCalc = function(attackingPokesapien,defendingPokesapien)
			return damageCalc(attackingPokesapien.level, attackingPokesapien.magicAttack, defendingPokesapien.magicDefense, 50)
		end,
		priority = 1
	},
}


function battle:enter(parent, player, enemy)
	self.player, self.enemy = player, enemy
	self.player:prepareForBattle()
	self.enemy:prepareForBattle()
	self.bg = cache.image "gfx/window/standard.png"
	self.stage = cache.image "gfx/battlescreen.png"
	self.encounterText = self.enemy:getEncounterText()
	self.text = ""
	self.turn = self.player
	self.wild = class.isinstance(enemy, WildPokesapien)
	
	self.intro = true
	self.texttimer = 5
	if self.wild then
		self.texttimer = 4
	end
	self.playerOffsetX = 250
	self.enemyOffsetX = -250
	self.playerSapienOffsetX = 250
	self.enemySapienOffsetX = -250
	self.playerSapienOffsetY = 0
	self.enemySapienOffsetY = 0
	
	
	self.T = true
	
end

function battle:leave()
end

function battle:update(dt)
	self.player.activeSapien:update(dt)
	self.enemy.activeSapien:update(dt)
	self.texttimer = math.max(self.texttimer - dt, 0)
	if self.skip and self.texttimer > 0 then
		self.texttimer = math.floor(self.texttimer) + 0.01
		self.skip = false
	end
	
	if self.intro then
		if self.wild then
			if self.texttimer == 0 then
				self.intro = false
			elseif self.texttimer > 3 then
				self.playerOffsetX = (self.texttimer - 3 )* 250 
				self.enemySapienOffsetX = (self.texttimer - 3 )* -250
			elseif self.texttimer > 2 then 
				self.playerOffsetX = 0
				self.enemySapienOffsetX = 0
				self.text = self.encounterText 			
			elseif self.texttimer > 1 then 
				self.text = "Go! "..self.player.activeSapien.name.."!"
				self.playerOffsetX = (2 - self.texttimer ) * -250
				self.playerSapienOffsetX = (self.texttimer - 1 )* 250
			else
				self.playerSapienOffsetX = 0
				self.enemySapienOffsetX = 0
			end
		else
			if self.texttimer == 0 then
				self.intro = false
			elseif self.texttimer > 4 then
				self.playerOffsetX = (self.texttimer - 4 )* 250 
				self.enemyOffsetX = (self.texttimer - 4 )* -250
			elseif self.texttimer > 3 then 
				self.playerOffsetX = 0
				self.enemyOffsetX = 0
				self.text = self.encounterText
			elseif self.texttimer > 2 then 
				self.text = self.enemy.name.." sent\nout "..self.enemy.activeSapien.name.."!"
				self.enemyOffsetX = (3 - self.texttimer ) * 250
				self.enemySapienOffsetX = (self.texttimer - 2 )* -250			
			elseif self.texttimer > 1 then 
				self.enemySapienOffsetX  = 0
				self.text = "Go! "..self.player.activeSapien.name.."!"
				self.playerOffsetX = (2 - self.texttimer ) * -250
				self.playerSapienOffsetX = (self.texttimer - 1 )* 250
			else
				self.playerSapienOffsetX = 0
				self.enemySapienOffsetX = 0
			end
		end
	elseif self.switchingHuman then
		if self.texttimer == 0 then
			self.switchingHuman = false
		elseif self.texttimer > 2 then
			self.text = self.player.activeSapien.name..", good!\nCome back!"
			self.playerSapienOffsetX = (3 - self.texttimer) * 250
		elseif self.texttimer > 1 then 
			self:switchHuman(self.player, self.nextHuman)
			self.text = "Go! "..self.player.activeSapien.name.."!"
			self.playerSapienOffsetX = (self.texttimer - 1 )* 250 
		else
			self.playerSapienOffsetX = 0
			self.enemySapienOffsetX = 0
		end
	elseif self.performingAttack then
		if self.texttimer == 0 then
			self.performingAttack = false
			self.attackEfectiveMsg = nil
			self.nextAbiity = nil
			if not self.enemy.activeSapien:isAlive()then
				self.enemyDead = true
				self.texttimer = 5
			else
				if self.priority ==  self.player then
					self.performingAttackEnemy = true
					self.texttimer = 3
				end
			end
		elseif self.texttimer > 2 then
			self.player.activeSapien.sway = true
			self.text = self.player.activeSapien.name.." used \n"..self.nextAbiity.name
		elseif self.texttimer > 1 then
			self.enemy.activeSapien.violentShake = true
			self.enemy.activeSapien.flash = true
			self.player.activeSapien.sway = false
			if self.attackEfectiveMsg == nil then
				self.attackEfectiveMsg = self:useAbility(self.player.activeSapien,self.enemy.activeSapien,self.nextAbiity)
			end
		else
			self.enemy.activeSapien.violentShake = false
			self.enemy.activeSapien.flash = false
			if self.attackEfectiveMsg ~= "" then
				self.text = self.attackEfectiveMsg
			end
		end
	elseif self.performingAttackEnemy then
		if self.texttimer == 0 then
			self.performingAttackEnemy = false
			self.attackEfectiveMsg = nil
			if not self.player.activeSapien:isAlive()then
				self.dead = true
				self.texttimer = 1
			else
				if self.nextAbiity ~= nil and self.priority ==  self.enemy then
					self.performingAttack = true
					self.texttimer = 3
				end
			end
		elseif self.texttimer > 2 then
			self.enemy.activeSapien.sway = true
			self.text = self.enemy.activeSapien.name.." used \n"..self.nextEnemyAbiity.name
		elseif self.texttimer > 1 then
			self.player.activeSapien.violentShake = true
			self.player.activeSapien.flash = true
			self.enemy.activeSapien.sway = false
			if self.attackEfectiveMsg == nil then
				self.attackEfectiveMsg = self:useAbility(self.enemy.activeSapien,self.player.activeSapien,self.nextEnemyAbiity)
			end
		else
			self.player.activeSapien.violentShake = false
			self.player.activeSapien.flash = false
			if self.attackEfectiveMsg ~= "" then
				self.text = self.attackEfectiveMsg
			end
		end
	elseif self.enemyDead then
		if self.texttimer == 0 then
			self.enemyDead = false
			self.nextHuman = self.enemy:firstSapienAlive()
			if self.nextHuman ~= nil then
				self.enemy.activeSapien = self.nextHuman 
				self.enemyReplaceHuman = true
				self.texttimer = 2
			else
				self.win = true
				self.texttimer = 2	
			end
			
		elseif self.texttimer > 4 then
			self.text = "Foe "..self.enemy.activeSapien.name.."\ndied!"
			self.enemySapienOffsetY = (3 - self.texttimer) * 250
		elseif self.texttimer > 2 then
			if self.text == "Foe "..self.enemy.activeSapien.name.."\ndied!" then
				self.text, self.lvlUpMsg = self.player.activeSapien:gainExp(self.enemy.activeSapien)
			end
		else
			if self.lvlUpMsg == nil then
				self.skip = true
			else
				self.text = self.lvlUpMsg
			end
		end
	elseif self.dead then
		if self.texttimer == 0 then
			self.dead = false
			self.nextHuman = self.player:firstSapienAlive()
			if self.nextHuman ~= nil then
				Gamestate.push(states.humonSelectionMenu, self.player)
			else
				self.lose = true
				self.texttimer = 2
			end
		else
			self.text = self.player.activeSapien.name.." died!"
			self.playerSapienOffsetY = (1 - self.texttimer) * 250
		end
	elseif self.enemyReplaceHuman then
		if self.texttimer == 0 then
			self.enemyReplaceHuman = false
			self.turn = self.player
			convoke(function(continue, wait)
				Gamestate.pushlate(states.msgbox, "Do you want to change Humon?", continue())
				wait()
				Gamestate.pushlate(states.question, continue())
				if wait() then
					self.humanSwitch = true
					Gamestate.push(states.humonSelectionMenu, self.player)
				end
			end)()
			
		else
			self.text = self.enemy.name.." sent\nout "..self.enemy.activeSapien.name.."!"
			self.enemySapienOffsetY = 0
			self.enemySapienOffsetX = (self.texttimer)* -250
		end
	elseif self.replacingHuman then
		if self.texttimer == 0 then
			self.replacingHuman = false
			self.turn = self.player
		else
			self.text = "Go! "..self.player.activeSapien.name.."!"
			self.playerSapienOffsetY = 0
			self.playerSapienOffsetX = (self.texttimer)* 250
		end
	elseif self.lose then
		if self.texttimer == 0 then
			self.lose = false
			states.world:teleportPlayer({map= "cloning", target = vector(9,9)})
			Gamestate.pop()
		else
			self.text = "All your humons are dead,\n Active Emergency Teleport!!"
			for i, v in ipairs(states.world.player.pokesapiens) do
				v.hp = v.maxHp
				v.displayHp = v.hp
				for j, w in ipairs(v.abilities) do
					w.pp = w.maxPP
				end
			end
		end	
	elseif self.win then
		if self.texttimer == 0 then
			self.win = false
			Gamestate.pop()
		else
			if self.wild then
				self.text = "The wild "..self.enemy.activeSapien.name.." has been killed"
			else
				self.text = "You are victorious in your Human battle against "..self.enemy.name
				self.enemy.defeated = true
				self.enemy:defeatCb()
			end
		end
	elseif self.captureFailed then
		if self.texttimer == 0 then
			self.captureFailed = false
		elseif self.texttimer > 2 then
			self.enemy.activeSapien.violentShake = true
		else
			self.enemy.activeSapien.violentShake = false
			self.text = self.enemy.activeSapien.name.." Escaped!"
		end
	elseif self.captureSuccess then
		if self.texttimer == 0 then
			self.captureSuccess = false
			self.player:captureSapien(self.enemy.activeSapien)
			Gamestate.pop()
		elseif self.texttimer > 2 then
			self.enemy.activeSapien.violentShake = true
		else
			self.enemy.activeSapien.violentShake = false
			self.text = "Succesfully captured "..self.enemy.activeSapien.name.." !"
		end
	elseif self.runFail then
		if self.texttimer == 0 then
			self.runFail = false
		elseif self.texttimer > 1 then
			--running
		else
			self.text = self.player.activeSapien.name.." failed to get away!"
		end	
	elseif self.runSuccess then
		if self.texttimer == 0 then
			self.runSuccess = false
			Gamestate.pop()
		elseif self.texttimer > 1 then
			--running
		else
			self.text = self.player.activeSapien.name.." got away!"
		end	
	else
		if self.texttimer == 0 then
			self.text = ""
			if self.turn == self.player then
				self.player.activeSapien.bob = true
				Gamestate.pushlate(states.battleMenu, self.player)
			else
				-- Enemy turn
				-- Enemy use ability
				
				self.nextEnemyAbiity = self.enemy.activeSapien.abilities[love.math.random(#self.enemy.activeSapien.abilities)]
				
				
				
				if self.nextAbiity ~= nil then
					local playerPriority = attackDetails[self.nextAbiity.name].priority
					local enemyPriority = attackDetails[self.nextEnemyAbiity.name].priority
					if playerPriority > enemyPriority then
						self.priority = self.player
					elseif playerPriority < enemyPriority then
						self.priority = self.enemy
					elseif self.player.activeSapien.agility < self.enemy.activeSapien.agility then
						self.priority = self.enemy
					else
						self.priority = self.player
					end
					
					if self.priority == self.enemy then
						self.performingAttackEnemy = true
						self.texttimer = 3
					else
						self.performingAttack = true
						self.texttimer = 3
					end
				else
					self.priority = self.enemy
					self.performingAttackEnemy = true
					self.texttimer = 3
				end
				self:nextTurn()
			end
		end
	end
end

function battle:resume(previousState, action, ...)
	self.player.activeSapien.bob = false
	
	if self.humanSwitch and previousState == states.humonSelectionMenu then
		self.humanSwitch = false
		self.turn = self.player
		self:playerSwitchHuman(action,...)
	elseif previousState == states.humonSelectionMenu then
		self:playerReplaceHuman(action,...)
		self:nextTurn()
	elseif action ==  "SwitchHuman" then
		self:playerSwitchHuman(...)
	elseif action == "UseAbility" then
		self:playerUseAbility(...)
		self:nextTurn()
	elseif action == "UseItem" then
		self:playerUseItem(...)
		
	elseif action == "Run" then
		self:attemptToRun(...)
		self:nextTurn()
	end

	
end

function battle:keypressed(key)
	if key == " " then
		self.skip = true
	end
end

function battle:nextTurn()
	self.turn = self.turn == self.player and self.enemy or self.player
end

function battle:playerReplaceHuman(pokesapien)
	if pokesapien == nil or not pokesapien:isAlive() then
		Gamestate.push(states.humonSelectionMenu, self.player)
	end
	self.player.activeSapien = pokesapien
	self.replacingHuman = true
	self.texttimer = 1
end

function battle:playerSwitchHuman(pokesapien)
	if  pokesapien ~= self.player.activeSapien and pokesapien:isAlive()then
		self:nextTurn()
		self.switchingHuman = true
		self.texttimer = 3
		self.nextHuman = pokesapien
	end
end

function battle:playerUseAbility(ability)
	--if not self.T then
	--	self.performingAttack = true
	--	self.texttimer = 3
	--end
	self.nextAbiity = ability
	--self:useAbility(self.player.activeSapien,self.enemy.activeSapien,ability)
end

function battle:playerUseItem(item)
	self:useItem(self.player.activeSapien,self.enemy.activeSapien,item)
end

function battle:switchHuman(trainer, pokesapien)
	trainer.activeSapien = pokesapien
end

function battle:useAbility(attackingPokesapien, defendingPokesapien, ability)
	--TODO proper damage calc
	ability.pp = ability.pp-1
	local attackInfo = attackDetails[ability.name]
	return defendingPokesapien:damage(ability.type, attackInfo.damageCalc(attackingPokesapien, defendingPokesapien))
end

function battle:useItem(attackingPokesapien, defendingPokesapien, item)
	local itemFunc = itemEffects[item.name]
	if itemFunc ~= nil then
		self:nextTurn()
		item.qty = item.qty-1
		self.text, self.texttimer = itemFunc(attackingPokesapien,defendingPokesapien,item)
	else
		
		self.text = item.name.." cannot be used during battle!"
		self.texttimer = 2
	end
	
	--assume its player
	if item.qty == 0 then
		for i, v in ipairs(self.player.items) do
			if v == item then
				table.remove(self.player.items,i)
			end
		end
	end
end

function battle:attemptToRun()
	if not self.wild then
		self:nextTurn() -- switch back so we dont skip turn
		self.text = "You cant run away from me you pussy"
		self.texttimer = 2
	else
		self.text = self.player.activeSapien.name.." attempts to flee the battle"
		if love.math.random(2) > 1 then 
			self.runFail = true
		else
			self.runSuccess = true
		end
		self.texttimer = 2
	end
end

function battle:draw()
	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(self.stage)
	
	if self.intro then
		self.enemy:drawInBattleEnemy(self.enemyOffsetX,0)
		self.player:drawInBattle(self.playerOffsetX,0)
	end
		
	self.enemy.activeSapien:drawInBattleEnemy(self.enemySapienOffsetX, self.enemySapienOffsetY)
	self.player.activeSapien:drawInBattle(self.playerSapienOffsetX, self.playerSapienOffsetY)
	
	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(self.bg, 0, HEIGHT-48)

	love.graphics.printfs(self.text, 5, HEIGHT-48+5, WIDTH-5, "left")
end

return battle
