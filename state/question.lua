local cache = require "lib.cache"
local msgbox = require "state.msgbox"

local question = {}

function question:enter(parent, cb)
	self.cb = cb
	self.bg = cache.image "gfx/window/battle.png"
	self.selection = 1
end

function question:leave()
	if self.cb then
		self.cb(self.selection == 1)
	end
end

function question:update(dt)
end

function question:draw()
	msgbox:draw()

	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(self.bg, 50, 50)

	local y = 60
	if self.selection == 2 then y = 70 end
	love.graphics.polygon('fill', 55, y-1, 55, y+1, 57, y)

	local x = 60
	if self.selection == 1 then x = x + math.sin(love.timer.getTime()*4) end
	love.graphics.prints("Yes", x, 55)
	x = 60
	if self.selection == 2 then x = x + math.sin(love.timer.getTime()*4) end
	love.graphics.prints("No", x, 65)
end

function question:keypressed(key)
	if key == "down" then
		self.selection = self.selection % 2 + 1
	elseif key == "up" then
		self.selection = (self.selection - 2) % 2 + 1
	elseif key == "return" then
		Gamestate.pop()
	end
end

return question
