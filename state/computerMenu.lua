local cache = require "lib.cache"
local convoke = require "lib.convoke"

local computerMenu = {}

function computerMenu:enter(parent, player)
	self.player = player
	self.selection = 1
	self.active = 1
	self.page = 1
	self.pages = math.ceil(#self.player.pokesapiensInStorage / 5)
	self.bg = cache.image "gfx/window/pokeSelect.png"
	self.bgInfo = cache.image "gfx/window/item.png"
end

function computerMenu:keypressed(key)
	if key == "escape" then
		Gamestate.poplate()
	elseif key == "down" then
		self.selection = self.selection % math.min(5, #self.player.pokesapiensInStorage - ((self.page-1) * 5) ) + 1
	elseif key == "up" then
		self.selection = (self.selection - 2) % math.min(5, #self.player.pokesapiensInStorage - ((self.page-1) * 5) ) + 1
	elseif key == "left" then
		self.page = self.page % self.pages + 1
		self.selection = 1
	elseif key == "right" then
		self.page = (self.page - 2) % self.pages + 1
		self.selection = 1
	elseif key == "return" then
		self:addToParty()
	end
end

function computerMenu:addToParty()
local humon = self.player.pokesapiensInStorage[((self.page-1) * 5) + self.selection]
if humon ~= nil then
	convoke(function(continue, wait)
				Gamestate.pushlate(states.msgbox, "Do you want to add "..humon.name.." to your party?", continue())
				wait()
				Gamestate.pushlate(states.question, continue())
				if wait() then
					if #self.player.pokesapiens < 6 then
						table.insert(self.player.pokesapiens,humon)
						table.remove(self.player.pokesapiensInStorage,((self.page-1) * 5) + self.selection)
						self.pages = math.ceil(#self.player.pokesapiensInStorage / 5)
						self.page = math.min(self.page,self.pages)
						self.selection = 1
					else
						Gamestate.pushlate(states.msgbox, "Your party is already full choose a Humon to replace", continue())
						wait()
						Gamestate.pushlate(states.humonSelectionMenu, states.world.player)
					end
				end
			end)()
end
end


function computerMenu:resume(from, pokesapien)
	if from == states.humonSelectionMenu and pokesapien then
		table.insert(self.player.pokesapiens,self.player.pokesapiensInStorage[((self.page-1) * 5) + self.selection])
		table.remove(self.player.pokesapiensInStorage,((self.page-1) * 5) + self.selection)
		table.insert(self.player.pokesapiensInStorage,pokesapien)
		for i, v in ipairs(self.player.pokesapiens) do
			if v == pokesapien then
				table.remove(self.player.pokesapiens,i)
			end
		end
	end
end

function computerMenu:draw()	
	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(self.bg)
	
	local skip = (self.page-1) * 5

	for i, v in ipairs(self.player.pokesapiensInStorage) do
		if i > skip then
			local x, y = 124
			local spriteX, spriteY
			y = 15 + 30*((i-skip)-1)
			v:drawHpBar(166, y+4, true)
			love.graphics.prints("Lv" .. v.level, x, y)
			love.graphics.prints(v.name, x, y+8)
			spriteX, spriteY = x-20, y+2

			love.graphics.setColor(255, 255, 255)
			love.graphics.draw(v.battleSpriteEnemy, spriteX, spriteY, 0, 1/4)

			if self.selection == (i-skip) then
				love.graphics.setColor(255, 255, 255)
				love.graphics.point(x-5, y+8)
				love.graphics.polygon('fill', x-7, y+6, x-7, y+10, x-3, y+8)
			end
		end
	end
	love.graphics.printfs(self.page.."/"..self.pages, WIDTH - 35, 1, 86, "left")
	love.graphics.printfs("Choose a Humon.", 5, HEIGHT-48+5, 86, "left")
	self:drawInfo()
end

function computerMenu:drawInfo()
	local humon = self.player.pokesapiensInStorage[((self.page-1) * 5) + self.selection]
	
	if humon ~= nil then
		love.graphics.draw(self.bgInfo, -12, 0)
		humon:drawInBattle(-116, -60, false)

		local y = 52
		local function skip()
			y = y + 10
		end
		local function print(txt)
			love.graphics.prints(txt, 3, y)
			skip()
		end

		print("XP: " .. humon.xp)
		print("Next level: " .. math.floor(humon.xpToNextLvl))
		skip()
		skip()

		print("Phys. Atk: " .. humon.attack)
		print("Phys. Def: " .. humon.defence)
		print("Magic Atk: " .. humon.magicAttack)
		print("Magic Def: " .. humon.magicDefense)
		print("Agility: " .. humon.agility)
	end
end

return computerMenu
