local cache = require "lib.cache"

local humonSelectionMenu = {}

function humonSelectionMenu:enter(parent, player)
	self.player = player

	local active = 1
	for i, v in ipairs(self.player.pokesapiens) do
		if v == self.player.activeSapien then
			active = i
		end
	end

	if not active then active = 1 end
	self.selection = 1
	self.active = 1

	self.player.pokesapiens[1], self.player.pokesapiens[active] = self.player.pokesapiens[active], self.player.pokesapiens[1]

	self.bg = cache.image "gfx/window/pokeSelect.png"
end

function humonSelectionMenu:keypressed(key)
	if key == "escape" then
		Gamestate.poplate()
	elseif key == "down" then
		self.selection = self.selection % #self.player.pokesapiens + 1
	elseif key == "up" then
		self.selection = (self.selection - 2) % #self.player.pokesapiens + 1
	elseif key == "return" then
		self:selectSapien()
	end
end

function humonSelectionMenu:selectSapien()
	Gamestate.poplate(self.player.pokesapiens[self.selection])
end

function humonSelectionMenu:draw()	
	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(self.bg)

	for i, v in ipairs(self.player.pokesapiens) do
		local x, y = 124
		local spriteX, spriteY
		if i == self.active then
			x, y = 17, 41
			v:drawHpBar(22, 65, true)
			love.graphics.prints(v.name, x, y)
			love.graphics.prints("Lv" .. v.level, x+5, y+10)
			spriteX, spriteY = x+50, y+3
		else
			y = 15 + 30*(i-1)
			if i > self.active then y = y - 30 end
			v:drawHpBar(166, y+4, true)
			love.graphics.prints("Lv" .. v.level, x, y)
			love.graphics.prints(v.name, x, y+8)
			spriteX, spriteY = x-20, y+2
		end

		love.graphics.setColor(255, 255, 255)
		love.graphics.draw(v.battleSpriteEnemy, spriteX, spriteY, 0, 1/4)

		if self.selection == i then
			love.graphics.setColor(255, 255, 255)
			love.graphics.point(x-5, y+8)
			love.graphics.polygon('fill', x-7, y+6, x-7, y+10, x-3, y+8)
		end
	end
	
	love.graphics.printfs("Choose a Humon.", 5, HEIGHT-48+5, 86, "left")
end

return humonSelectionMenu
