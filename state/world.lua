local sti = require "lib.sti"
local convoke = require "lib.convoke"
local cache = require "lib.cache"
local inifile = require "lib.inifile"
local fix = require "util.resolutionfix"
local pokeparse = require "util.pokeparse"

local world = {}

local function splitString(str)
	local t = {}
	for w in str:gmatch("[^,]+") do
		table.insert(t, w)
	end
	return t
end

function world:enter()
	self.player = Player(22, 9)
	self.trainers = {}
	self.defeatedTrainers = {}
	self.teleports = {}

	self:loadMap("overworld")

	self.timer = Timer.new()

	self.exclaim =
	{
		image = cache.image("gfx/exclamation.png"),
		frames =
		{
			love.graphics.newQuad(0, 0, 16, 16, 51, 17),
			love.graphics.newQuad(17, 0, 16, 16, 51, 17),
			love.graphics.newQuad(34, 0, 16, 16, 51, 17),
		},

		frame = 1,
		drawn = nil,
	}

	self.cages = {}
	self.vats = {}
end

function world:loadMap(map)
	self.map = sti.new("map/" .. map .. ".lua")
	self.currentMap = map

	self.map.layers["Trainers"].visible = false
	for i, v in ipairs(self.map.layers["Trainers"].objects) do
		local trainer = AITrainer(v.type, math.floor(v.x/16)+1, math.floor((v.y+8)/16)+1)
		trainer.name = v.name
		trainer.inMap = map

		local dialog = {}
		for j = 1, math.huge do
			dialog[j] = v.properties["dialog" .. j]
			if not dialog[j] then break end
		end

		if dialog[1] then
			trainer.dialog = dialog
		end

		if v.properties.behaviour then
			trainer.behaviour = v.properties.behaviour
		end

		for j, w in pairs(v.properties) do
			if j:match("^beh_") then
				trainer[j] = w
			end
		end

		if v.properties.pokesapiens then
			local pokes = splitString(v.properties.pokesapiens)
			trainer.pokesapiens = {}
			for j, w in ipairs(pokes) do
				table.insert(trainer.pokesapiens, Pokesapien(w))
			end
		end

		trainer.quest = v.properties.quest
		if v.properties.range then
			trainer.range = tonumber(v.properties.range)
		end

		trainer.vat = v.properties.vat
		trainer.wallhacking = v.properties.wallhacking
		trainer.endboss = v.properties.endboss

		if self.defeatedTrainers[v.name] then
			trainer.defeated = true
		end

		if not self.trainers[v.name] then
			self.trainers[v.name] = trainer
		end
	end

	self.teleports = {}
	self.map.layers["Teleports"].visible = false
	for i, v in ipairs(self.map.layers["Teleports"].objects) do
		local tp = {}
		tp.position = vector(math.floor(v.x/16)+1, math.floor(v.y/16)+1)

		local targetX, targetY = v.properties.target:match("(%d+),(%d+)")
		tp.target = vector(tonumber(targetX), tonumber(targetY))

		tp.map = v.properties.map or self.currentMap

		table.insert(self.teleports, tp)
	end

	self.signs = {}
	if self.map.layers["Signs"] then
		self.map.layers["Signs"].visible = false
		for i, v in ipairs(self.map.layers["Signs"].objects) do
			local sign = {}
			sign.position = vector(math.floor(v.x/16)+1, math.floor(v.y/16)+1)
			sign.text = v.properties.text

			table.insert(self.signs, sign)
		end
	end
end

function world:leave()
	self.player = nil
	self.enemy = nil
end

function world:update(dt)
	self.timer.update(dt)
	self.map:update(dt)

	local movement
	if not self.player.moving and not self.exclaim.drawn and not self.player.frozen then
		if love.keyboard.isDown("up", "w") then
			movement = vector(0, -1)
		elseif love.keyboard.isDown("down", "s") then
			movement = vector(0, 1)
		elseif love.keyboard.isDown("left", "a") then
			movement = vector(-1, 0)
		elseif love.keyboard.isDown("right", "d") then
			movement = vector(1, 0)
		end
	end

	if movement then
		self.player:move(movement)
	end

	if not self.frozen then
		self.player:update(dt)
		for i, v in pairs(self.trainers) do
			if v.inMap == self.currentMap then
				v:update(dt)
			end
		end
	end

	for i = #self.cages, 1, -1 do
		if self.cages[i].dead then
			table.remove(self.cages, i)
		end
	end

	for i = #self.vats, 1, -1 do
		self.vats[i]:update(dt)
		if self.vats[i].dead then
			table.remove(self.vats, i)
		end
	end
end

function world:tilesAt(x, y)
	return coroutine.wrap(function()
		for i, v in ipairs(self.map.layers) do
			if v.type == "tilelayer" then
				if v.data[y] and v.data[y][x] then
					coroutine.yield(v.data[y][x])
				end
			end
		end
	end)
end

function world:trainerAt(x, y)
	local pos = vector(x, y)
	for i, v in pairs(self.trainers) do
		if v.worldPosition == pos and v.inMap == self.currentMap then
			return true
		end
	end
	return false
end

function world:cageAt(x, y)
	local pos = vector(x, y)
	for i, v in ipairs(self.cages) do
		if v.worldPosition == pos and v.inMap == self.currentMap then
			return true
		end
	end
	return false
end

function world:teleportAt(pos)
	for i, v in ipairs(self.teleports) do
		if v.position == pos then
			return v
		end
	end
end

function world:signAt(pos)
	for i, v in ipairs(self.signs) do
		if v.position == pos then
			return v
		end
	end
end

function world:isSolid(x, y)
	for tile in self:tilesAt(x, y) do
		if tile.properties and tile.properties.solid then
			return true
		end
	end

	return false
end

function world:canMoveTo(x, y)
	return not world:isSolid(x, y) and not world:cageAt(x, y) and not world:trainerAt(x, y) and not world:isPlayer(x, y)
end

function world:blocksView(x, y)
	-- solid or blocksView properties
	for tile in self:tilesAt(x, y) do
		if tile.properties and (tile.properties.solid or tile.properties.blocksView) then
			return true
		end
	end

	return false
end

function world:hasRandomEncounters(x, y)
	for tile in self:tilesAt(x, y) do
		if tile.properties and tile.properties.randomEncounters then
			return tile.properties.randomEncounters
		end
	end

	return false
end

function world:getRandomEncounter()
	local encounters = self:hasRandomEncounters(self.player.worldPosition:unpack())
	if not encounters then return end
	if love.math.random() > 0.05 then return end -- 5% chance

	encounters = splitString(encounters)
	return WildPokesapien(Pokesapien(encounters[love.math.random(#encounters)]))
end

function world:isTrainer(x, y)
	for i, v in pairs(self.trainers) do
		if v.worldPosition.x == x and v.worldPosition.y == y and not v.moving and v.inMap == self.currentMap then
			return v
		end
	end
end

function world:isPlayer(x, y)
	return self.player.worldPosition.x == x and self.player.worldPosition.y == y and not self.player.moving
end

function world:getPlayerDistance(x, y)
	return math.abs(x-self.player.worldPosition.x, y-self.player.worldPosition.y)
end

function world:encounter(trainer)
	if #self.player.pokesapiens == 0 then return end -- Player needs pokesapiens first
	if self.exclaim.drawn then return end
	convoke(function(continue, wait)
		self.frozen = true
		self.exclaim.drawn = trainer.worldPosition*16 - vector(10, 28)
		self.exclaim.frame = 1
		self.timer.add(0.2, continue())
		wait()

		self.exclaim.frame = 2
		self.timer.add(0.2, continue())
		wait()

		self.exclaim.frame = 3
		self.timer.add(0.5, continue())
		wait()

		self.exclaim.drawn = nil
		for i, v in ipairs(trainer.dialog or {}) do
			Gamestate.pushlate(states.msgbox, v, continue())
			wait()
		end

		Gamestate.pushlate(states.battle, self.player, trainer)
		self.frozen = false
	end)()
end

function world:keypressed(key)
	if key == "escape" then
		Gamestate.push(states.menu)
	elseif key == "f3" then
		self:save()
	elseif key == "f4" then
		self:load()
	elseif key == "return" then
		local pos = self.player.worldPosition + self.player.facing
		for i, v in ipairs(self.cages) do
			if v.worldPosition == pos then
				v:activate(self.player)
				break
			end
		end
		local sign = self:signAt(pos)
		if sign then
			Gamestate.push(states.msgbox, sign.text)
		end
	end
end

function world:draw()
	local pos = self.player:getDrawPosition()
	love.graphics.push()
	love.graphics.translate(-pos.x*16+WIDTH/2, -pos.y*16+HEIGHT/2)

	self.map.drawOff = {x = -pos.x*16+WIDTH/2, y = -pos.y*16+HEIGHT/2}
	self.map:draw()

	fix.scissor(true)
	for i, v in pairs(self.trainers) do
		if v.inMap == self.currentMap then
			v:drawInWorld()
		end
	end

	for i, v in ipairs(self.cages) do
		if v.inMap == self.currentMap then
			v:drawInWorld()
		end
	end

	for i, v in ipairs(self.vats) do
		v:drawInWorld()
	end
	fix.scissor(false)

	self.player:drawInWorld()

	if self.exclaim.drawn then
		love.graphics.setColor(255, 255, 255)
		love.graphics.draw(self.exclaim.image, self.exclaim.frames[self.exclaim.frame], self.exclaim.drawn:unpack())
	end

	love.graphics.pop()
end

function world:teleportPlayer(tp)
	if tp.map ~= self.currentMap then
		self:loadMap(tp.map)
	end

	self.player.worldPosition = tp.target
	self.player.drawPosition = nil
end

function world:save()
	-- FIXME items
	local activeSapien = 1
	for i, v in ipairs(self.player.pokesapiens) do
		if self.player.activeSapien == v then
			activeSapien = i
			break
		end
	end

	local data =
	{
		player =
		{
			x = self.player.worldPosition.x,
			y = self.player.worldPosition.y,
			activeSapien = activeSapien,
			map = self.currentMap,
		},

		-- pokesapien1, pokesapien2, etc
		-- item1, item2, etc

		trainers =
		{
			-- trainername = true/false (defeated)
		},
	}

	for i, v in ipairs(self.player.pokesapiens) do
		data["pokesapien" .. i] = pokeparse.save(v)
	end

	for i, v in ipairs(self.player.pokesapiensInStorage) do
		data["storedPokesapien" .. i] = pokeparse.save(v)
	end

	for i, v in pairs(self.trainers) do
		local storeName = v.name:gsub("%s", "")
		data.trainers[storeName] = v.defeated
	end

	for i, v in ipairs(self.player.items) do
		data["item" .. i] =
		{
			name = v.name,
			qty = v.qty
		}
	end

	inifile.save("save.ini", data)

	Gamestate.pushlate(states.msgbox, "Game saved!")
end

function world:load()
	local succ, data = pcall(inifile.parse, "save.ini")
	if not succ then
		Gamestate.pushlate(states.msgbox, "Failed to read save, does it exist?")
		return
	end

	self:teleportPlayer{target = vector(data.player.x, data.player.y), map = data.player.map}
	self.player.pokesapiens = {}
	self.player.pokesapiensInStorage = {}

	for i = 1, 6 do
		local v = data["pokesapien" .. i]
		if not v then break end
		local pok = Pokesapien()
		self.player.pokesapiens[i] = pokeparse.parse(pok, v)
		pok:setStats()
	end

	for i = 1, math.huge do
		local v = data["storedPokesapien" .. i]
		if not v then break end
		local pok = Pokesapien()
		self.player.pokesapiensInStorage[i] = pokeparse.parse(pok, v)
		pok:setStats()
	end

	self.player.activeSapien = self.player.pokesapiens[data.player.activeSapien]

	for i, v in pairs(self.trainers) do
		local storeName = v.name:gsub("%s", "")
		v.defeated = data.trainers[storeName] or false
	end

	for i, v in pairs(data.trainers) do
		self.defeatedTrainers[i] = v
	end

	self.player.items = {}
	for i = 1, math.huge do
		local v = data["item" .. i]
		if not v then break end

		self.player.items[i] =
		{
			name = v.name,
			qty = v.qty,
		}
	end

	Gamestate.pushlate(states.msgbox, "Save game loaded")
end

return world
