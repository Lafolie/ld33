local cache = require "lib.cache"

local msgbox = {}

msgbox.speed = 8
msgbox.closeDelay = msgbox.speed*2

function msgbox:enter(parent, text, cb)
	self.parent, self.text = parent, text
	self.time = #text + self.closeDelay
	self.characters = 0
	self.skipped = false
	self.cb = cb

	self.bg = cache.image "gfx/window/standard.png"
end

function msgbox:leave()
	if self.cb then
		self.cb()
	end
end

function msgbox:update(dt)
	self.time = self.time - dt*self.speed
	self.characters = self.characters + dt*self.speed

	if self.time <= 0 then
		Gamestate.poplate()
	end
end

function msgbox:keypressed(key)
	if key == " " then
		if self.skipped then
			return Gamestate.pop()
		end

		self.characters = #self.text
		self.time = self.closeDelay
		self.skipped = true
	end
end

function msgbox:draw()
	self.parent:draw()

	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(self.bg, 0, HEIGHT-48)

	local txt = self.text:sub(1, self.characters)

	-- Drop shadow
	love.graphics.printfs(txt, 5, HEIGHT-48+5, WIDTH-5, "left")

	local bounce = math.sin(self.time/2)
	if self.characters < #self.text then
		-- Down triangle
		love.graphics.polygon('fill', 233, 153+bounce, 235, 153+bounce, 234, 155+bounce)
	else
		-- Box
		love.graphics.polygon('fill', 233, 153+bounce, 235, 153+bounce, 235, 155+bounce, 233, 155+bounce)
	end
end

return msgbox
