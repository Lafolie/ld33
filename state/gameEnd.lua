local cache = require "lib.cache"

local gameEnd = {}

function gameEnd:enter()
	self.bg = cache.image "gfx/win.png"
end

function gameEnd:keypressed(key)
	if key == "return" then
		love.event.quit()
	end
end

function gameEnd:draw()
	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(self.bg)
end

return gameEnd
