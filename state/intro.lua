local convoke = require "lib.convoke"
local cache = require "lib.cache"
local fix = require "util.resolutionfix"

local intro = {}

function intro:enter()
	self.timer = Timer.new()

	self.opacity = 255
	self.wiping = true
	self.wipex = 0

	self.image = cache.image "gfx/logo.png"

	convoke(function(continue, wait)
		self.timer.tween(2, self, {wipex = WIDTH},
				"linear", continue())
		wait()
		self.wiping = false

		self.image = cache.image "gfx/splash.png"
		self.opacity = 0
		self.timer.tween(3, self, {opacity = 255},
				"quart", continue())
		wait()

		self.waitCb = continue()
		wait()

		self.timer.add(0.3, continue())
		wait()

		self.timer.tween(1, self, {opacity = 0},
				"out-quart", continue())
		wait()

		self:finish()
	end)()
end

function intro:update(dt)
	self.timer.update(dt)
end

function intro:finish()
	Gamestate.switch(states.world)
end

function intro:keypressed(key)
	if self.waitCb then
		self.waitCb()
	end
end

function intro:draw()
	if self.wiping then
		fix.scissor(true)
		love.graphics.setColor(255, 50, 50)
		love.graphics.rectangle('fill', 0, 0, self.wipex, HEIGHT)
		love.graphics.setColor(255, 100, 100)
		love.graphics.rectangle('fill', self.wipex, 0, WIDTH, HEIGHT)
		fix.scissor(false)
		love.graphics.setBlendMode("multiplicative")
	end
	love.graphics.setColor(255, 255, 255, self.opacity)
	love.graphics.draw(self.image)

	love.graphics.setBlendMode("alpha")
end

return intro
