local cache = require "lib.cache"

local fightMenu = {}

function fightMenu:enter(parent, pokesapien)
	self.parent = parent
	self.pokesapien = pokesapien
	self.selection = 1
	
	self.bg1 = cache.image "gfx/window/standard.png"
	self.bg2 = cache.image "gfx/window/battle.png"
end


function fightMenu:keypressed(key)
	if key == "escape" then
		Gamestate.poplate()
	elseif key == "down" then
		self.selection = self.selection % #self.pokesapien.abilities + 1
	elseif key == "up" then
		self.selection = (self.selection - 2) % #self.pokesapien.abilities + 1
	elseif key == "left" or key == "right" then
		self.selection = (self.selection - 3) % #self.pokesapien.abilities + 1
	elseif key == "return" then
		self:selectAbility()
		
	end
end

function fightMenu:selectAbility()
	if self.pokesapien.abilities[self.selection].pp > 0 then 
		Gamestate.poplate(self.pokesapien.abilities[self.selection])
	end
end

function fightMenu:draw()
	self.parent.parent:draw()
	

	
	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(self.bg1, 0, HEIGHT-48)
	love.graphics.draw(self.bg2, WIDTH - 96, HEIGHT-48)

	love.graphics.setColor(0, 0, 0)
	for i, v in ipairs(self.pokesapien.abilities) do
		if self.selection == i then
			local offset = math.sin(love.timer.getTime()*4)
			love.graphics.setColor(0, 0, 0)
			love.graphics.polygon('fill', (i > 2 and (WIDTH/3)-15 or 0) + 6, ((i+1)%2)*20 + 123,
			(i > 2 and (WIDTH/3)-15 or 0) + 6, ((i+1)%2)*20 + 133,
			(i > 2 and (WIDTH/3)-15 or 0) + 11, ((i+1)%2)*20 + 128)
			
			love.graphics.setColor(255, 255, 255)
			
			love.graphics.polygon('fill', (i > 2 and (WIDTH/3)-15 or 0) + 5, ((i+1)%2)*20 + 122,
			(i > 2 and (WIDTH/3)-15 or 0) + 5, ((i+1)%2)*20 + 132,
			(i > 2 and (WIDTH/3)-15 or 0) + 10, ((i+1)%2)*20 + 127)
			love.graphics.prints("PP "..v.pp.."/"..v.maxPP, WIDTH - 90 , 122)
				love.graphics.prints("Type/"..v.type, WIDTH - 90 , 142)
			if v.pp > 0 then
				love.graphics.prints(v.name, (i > 2 and (WIDTH/3)-15 or 0) + 10 + offset , ((i+1)%2)*20 + 122)
			else
				love.graphics.printd(v.name, (i > 2 and (WIDTH/3)-15 or 0) + 10 + offset , ((i+1)%2)*20 + 122)
			end
			love.graphics.prints("PP "..v.pp.."/"..v.maxPP, WIDTH - 90 , 122)
			love.graphics.prints("Type/"..v.type, WIDTH - 90 , 142)
		else
			if v.pp > 0 then
				love.graphics.prints(v.name, (i > 2 and (WIDTH/3)-15 or 0) + 10, ((i+1)%2)*20 + 122)
			else
				love.graphics.printd(v.name, (i > 2 and (WIDTH/3)-15 or 0) + 10, ((i+1)%2)*20 + 122)
			end
			
		end
	end
end

return fightMenu
