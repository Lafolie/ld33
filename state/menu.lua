local cache = require "lib.cache"

local menu = {}

local options, mainOptions, settingsOptions

mainOptions =
{
	{ "Resume", function() end },
	{ "Humons", function() Gamestate.pushlate(states.humonSelectionMenu, states.world.player) return true end },
	{ "Items", function() Gamestate.pushlate(states.itemMenuWorld, states.world.player) return true end },
	{ "Computer", function() Gamestate.pushlate(states.computerMenu, states.world.player) return true end },
	{ "Save", function() states.world:save() end },
	{ "Load", function() states.world:load() end },
	{ "Settings", function() options = settingsOptions menu.selection = 1 return true end },
	{ "Quit", love.event.quit },
}

settingsOptions =
{
	{ "Fullscreen", function() love.window.setMode(0, 0, {fullscreen = true, fullscreentype = "desktop"}) love.resize(love.graphics.getDimensions()) end },
	{ "Windowed", function() love.window.setMode(WIDTH*4, HEIGHT*4, {fullscreen = false, resizable = true}) love.resize(love.graphics.getDimensions()) end },
	{ "Back", function() options = mainOptions menu.selection = 1 return true end },
}

function menu:enter(parent)
	self.parent = parent
	self.selection = 1

	self.bg = cache.image "gfx/window/pauseMenu.png"
	options = mainOptions
end

function menu:resume(from, pokesapien)
	if from == states.humonSelectionMenu and pokesapien then
		states.world.player.activeSapien = pokesapien
		Gamestate.push(from, states.world.player)
		return Gamestate.push(states.humonInfo, pokesapien)
	end
	Gamestate.pop()
end

function menu:keypressed(key)
	if key == "escape" then
		Gamestate.poplate()
	elseif key == "down" then
		self.selection = self.selection % #options + 1
	elseif key == "up" then
		self.selection = (self.selection - 2) % #options + 1
	elseif key == "return" then
		if not options[self.selection][2]() then
			Gamestate.poplate()
		end
	end
end

function menu:draw()
	self.parent:draw()

	local height = 20 + #options*20

	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(self.bg)

	local offset = 0
	for i, v in ipairs(options) do
		if self.selection == i then
			offset = math.sin(love.timer.getTime()*4)
			love.graphics.setColor(0, 0, 0)
			love.graphics.polygon('fill', 8, i*16-4, 8, i*16, 11, i*16-2)
			love.graphics.setColor(255, 255, 255)
			love.graphics.polygon('fill', 7, i*16-5, 7, i*16-1, 10, i*16-3)
		else
			offset = 0
		end
		love.graphics.prints(v[1], 12+offset, i*16-8)
	end
end

return menu
