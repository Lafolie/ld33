local cache = require "lib.cache"

local battleMenu = {}

battleMenu.options =
{
	{ "FIGHT", function() Gamestate.push(states.fightMenu, battleMenu.player.activeSapien) end },
	{ "HUMONS", function() Gamestate.push(states.humonSelectionMenu, battleMenu.player) end  },
	{ "BAG", function()Gamestate.push(states.itemMenu, battleMenu.player) end  },
	{ "RUN", function() battleMenu:AttemptToRun() end  },
}

battleMenu.msgSpeed = 32

function battleMenu:enter(parent, player)
	self.parent = parent
	self.player = player
	self.selection = 1
	self.bg = cache.image "gfx/window/battle.png"
	
	self.characters = 0
	self.text = "What will\n"..player.activeSapien.name.." do?"
end

function battleMenu:resume(previousState, ...)
	if previousState == states.humonSelectionMenu then
		self:resumeHumanSelection(...)
	elseif previousState == states.fightMenu then
		self:resumeFightSelection(...)
	elseif previousState == states.itemMenu then
		self:resumeItemSelection(...)
	end
end

function battleMenu:resumeHumanSelection(pokesapien)
	if pokesapien ~= nil then
		Gamestate.pop("SwitchHuman", pokesapien)
	end
end

function battleMenu:resumeFightSelection(ability)
	if ability ~= nil then
		Gamestate.pop("UseAbility", ability)
	end
end

function battleMenu:resumeItemSelection(item)
	if item ~= nil then
		Gamestate.pop("UseItem", item)
	end
end

function battleMenu:AttemptToRun()
	Gamestate.pop("Run")
end


function battleMenu:update(dt)
	self.characters = self.characters + dt*self.msgSpeed
end

function battleMenu:keypressed(key)
	if key == "escape" then
		Gamestate.poplate()
	elseif key == "down" then
		self.selection = self.selection % #self.options + 1
	elseif key == "up" then
		self.selection = (self.selection - 2) % #self.options + 1
	elseif key == "left" or key == "right" then
		self.selection = (self.selection - 3) % #self.options + 1
	elseif key == "return" then
		self.options[self.selection][2]()
		
	end
end

function battleMenu:draw()
	self.parent:draw()
	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(self.bg, WIDTH - 96, HEIGHT-48)
	
	local txt = self.text:sub(1, self.characters)

	love.graphics.setColor(0, 0, 0)
	for i, v in ipairs(self.options) do
		if self.selection == i then
			local offset = math.sin(love.timer.getTime()*4)
			love.graphics.setColor(0, 0, 0)
			love.graphics.polygon('fill', (i > 2 and WIDTH-30 or WIDTH-85 ) - 4, ((i+1)%2)*20 + 123,
			(i > 2 and WIDTH-30 or WIDTH-85 ) - 4, ((i+1)%2)*20 + 133,
			(i > 2 and WIDTH-30 or WIDTH-85) +  1, ((i+1)%2)*20 + 128)
			
			love.graphics.setColor(255, 255, 255)
			love.graphics.polygon('fill', (i > 2 and WIDTH-30 or WIDTH-85 ) - 5, ((i+1)%2)*20 + 122,
			(i > 2 and WIDTH-30 or WIDTH-85 ) - 5, ((i+1)%2)*20 + 132,
			(i > 2 and WIDTH-30 or WIDTH-85), ((i+1)%2)*20 + 127)
			love.graphics.prints(v[1], (i > 2 and WIDTH-30 or WIDTH-85) + offset , ((i+1)%2)*20 + 122)
			love.graphics.printfs(txt, 5, HEIGHT-48+5, WIDTH-5, "left")
		else
			love.graphics.prints(v[1], (i > 2 and WIDTH-30 or WIDTH-85), ((i+1)%2)*20 + 122)
		end
	end
end

return battleMenu
