local cache = require "lib.cache"

local humonInfo = {}

function humonInfo:enter(prev, humon)
	self.previous, self.humon = prev, humon

	self.bg = cache.image "gfx/window/item.png"
end

function humonInfo:keypressed(key)
	if key == " " or key == "return" then
		Gamestate.pop()
	end
end

function humonInfo:draw()
	self.previous:draw()

	love.graphics.draw(self.bg, 100, 0)
	self.humon:drawInBattle(-7, -60, false)

	local y = 52
	local function skip()
		y = y + 10
	end
	local function print(txt)
		love.graphics.prints(txt, 105, y)
		skip()
	end

	print("XP: " .. self.humon.xp)
	print("Next level: " .. math.floor(self.humon.xpToNextLvl))
	skip()
	skip()

	print("Phys. Atk: " .. self.humon.attack)
	print("Phys. Def: " .. self.humon.defence)
	print("Meta Atk: " .. self.humon.magicAttack)
	print("Meta Def: " .. self.humon.magicDefense)
	print("Agility: " .. self.humon.agility)
end

return humonInfo
