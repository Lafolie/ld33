function love.conf(t)
	t.window.width = 960
	t.window.height = 640
	t.window.resizable = true
	t.console = true
end
